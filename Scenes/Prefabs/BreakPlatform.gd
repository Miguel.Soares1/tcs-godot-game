extends KinematicBody2D





func _on_BreakPlatform_body_entered(body):
	if body.name == "Player":
		yield(get_tree().create_timer(2), "timeout")
		$AnimationPlayer.play("Destroyed")
		yield(get_node("AnimationPlayer"),"animation_finished")
		queue_free()
