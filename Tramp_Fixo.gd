extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_Area2D_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	body.velocity.y = body.jump_force / 1.30
	$anim.play("Jump")
	$Jump.play()
	var timer = get_tree().create_timer(0.5)
	yield(timer,"timeout")
	$Jump.stop()

