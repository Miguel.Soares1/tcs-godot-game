extends Node2D

onready var level1=load("res://Scenes/Levels/Level 9.tscn")
onready var level2=load("res://Scenes/Levels/Level16.tscn")
onready var level3=load("res://Scenes/Levels/Level10.tscn")
onready var level4=load("res://Scenes/Levels/Level17.tscn")
onready var level5=load("res://Scenes/Levels/Level11.tscn")
onready var level6=load("res://Scenes/Levels/Level18.tscn")
onready var level7=load("res://Scenes/Levels/Level12.tscn")
onready var level8=load("res://Scenes/Levels/Level 7.tscn")
onready var level9=load("res://Scenes/Levels/Level19.tscn")  
onready var level10=load("res://Scenes/Levels/Level21.tscn")
onready var level11=load("res://Scenes/Levels/Level6.tscn")
onready var level12=load("res://Scenes/Levels/Level 8.tscn")
onready var level13=load("res://Scenes/Levels/Level13.tscn")
onready var level14=load("res://Scenes/Levels/Level20.tscn")
onready var level15=load("res://Scenes/Levels/Level14.tscn")
onready var level16=load("res://Scenes/Levels/Level1.tscn")
onready var level17=load("res://Scenes/Levels/Level4.tscn")
onready var level18=load("res://Scenes/Levels/Level3.tscn")
onready var level19=load("res://Scenes/Levels/Level15.tscn")
onready var level20=load("res://Scenes/Levels/Level 5.tscn")
onready var level21=load("res://Scenes/Levels/Level22.tscn")
onready var Tutorial=load("res://Scenes/UI/Mensagem.tscn")

func _ready():
	$Levels/"1".grab_focus()
	for level in $Levels.get_children():
		if str2var(level.name) in range(Global.unlockedLevels+1):
			level.disabled = false
		else:
			level.disabled = true
	$CanvasLayer/anime.play_backwards("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	

		
		
func change_level(lvl_no):
	get_tree().change_scene("res://Scenes/Levels/Level"+ lvl_no  + ".tscn")

	
	
func _on_Button_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	get_tree().change_scene("res://Scenes/UI/TileScreen.tscn")


func _on_1_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.lower_music2()
	var timer = get_tree().create_timer(1)
	yield(timer,"timeout")
	get_tree().change_scene_to(Tutorial)


func _on_2_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level2)


func _on_3_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level3)


func _on_4_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level4)


func _on_5_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level5)
	

func _on_6_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level6)



func _on_7_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level7)
	
func _on_8_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level8)


func _on_9_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level9)

func _on_10_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level10)
	
func _on_11_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level11)

func _on_12_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level12)
	

func _on_13_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level13)

func _on_14_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level14)
	
func _on_15_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level15)
	

func _on_16_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level16)

func _on_17_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level17)


func _on_TextureButton_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	get_tree().change_scene("res://Scenes/UI/TileScreen.tscn")


func _on_18_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level18)
	

func _on_19_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level19)


func _on_20_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level20)


func _on_21_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	Music1.play_music()
	get_tree().change_scene_to(level21)

