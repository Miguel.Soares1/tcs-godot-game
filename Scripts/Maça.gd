extends Area2D

func _on_Maa_body_entered(body):
	$AnimationPlayer.play("collected")
	if body.has_method("cenouraPickUp"):
		body.cenouraPickUp()


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "collected":
		queue_free()
