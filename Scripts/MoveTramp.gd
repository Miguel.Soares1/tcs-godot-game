extends KinematicBody2D

var velocity = Vector2.ZERO
var jump_force = -600 
var object = 0
var gravity = 200
var is_grounded
var UP = Vector2.UP
var  speed = Vector2.AXIS_X * get_floor_velocity()

onready var raycasts = $raycasts


var snap = Vector2.DOWN * 12


func _ready():
	pass


func _on_Area2D_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if body.name == "Player":
		body.velocity.y = body.jump_force / 1.30
		$anim.play("Jump")
		$Jump.play()
		var timer = get_tree().create_timer(0.5)
		yield(timer,"timeout")
		$Jump.stop()


func _physics_process(delta):
	object = gravity * delta
	
	
	if object > 1:
		object = 1
		
	
	#velocity = move_and_slide_with_snap(Vector2(0,gravity * object),snap)
	velocity = move_and_slide_with_snap(Vector2(0,gravity * object),snap, Vector2.UP)  
	#move_and_slide_with_snap(velocity, snap, UP)
	#move_and_slide(speed)
	
	#move_and_slide(Vector2(get_floor_velocity(),gravity * object)
	
#func _check_is_ground():
#	for raycast in raycasts.get_children():
#		if raycast.is_colliding(): 
#			return true
#	return false



#func _physics_process(delta):
#	object = gravity * delta
#
#
#	if object > 1:
#		object = 1
#
#
#	#velocity = move_and_slide_with_snap(Vector2(0,gravity * object),snap)
#	velocity = move_and_slide_with_snap(Vector2(0,gravity * object),snap, Vector2.UP)  
#

