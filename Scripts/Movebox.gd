extends KinematicBody2D

var object = 0
var gravity = 300
var velocity = Vector2()
var jump_force = -600 
var snap = Vector2.DOWN * 18


func _ready():
	pass 
	
func _physics_process(delta):
	object = gravity * delta

	if object > 1:
		object = 1

	velocity = move_and_slide_with_snap(Vector2(0,gravity  * object),snap, Vector2.UP)  

