extends KinematicBody2D

export var player_death:PackedScene
var UP = Vector2.UP
var velocity = Vector2.ZERO
var move_speed = 115
const aceleration = 100  #default 0
var gravity = 1100  #1100 default
var jump_force = -655  #-655
var is_grounded
var is_pushing = false
var facing_right = true
var snap : Vector2
var JumpAvailability : bool
var teste = "ola"

onready var dust_particles_place_holder = get_node("DustParticlesPlaceHolder")
onready var dust_particles_scene = preload("res://Scenes/Player/Particles2D.tscn")

onready var coyote_timer = $CoyoteTimer
onready var raycasts = $raycasts

func _physics_process(delta: float) -> void:
	velocity.y += gravity * delta
	
	velocity.x = 0
	velocity.x = clamp(velocity.x, -move_speed, move_speed)
	if is_grounded:
		JumpAvailability = true
	elif JumpAvailability == true && coyote_timer.is_stopped():
		coyote_timer.start()
	
	
	
	if Input.is_action_pressed("move_right"):
		velocity.x += aceleration
		facing_right = true
		if is_pushing == false:
			$texture.play("Walk")
	elif Input.is_action_pressed("move_left"):
		velocity.x -= aceleration
		facing_right = false
		if is_pushing == false:
			$texture.play("Walk")
	else:
		$texture.play("Idle")
		velocity.x = lerp(velocity.x,0,0.25)
	#print(velocity.x)
	if  velocity.y < 0 && !is_on_floor():
		$texture.play("Jump")
	elif velocity.y > 0 && !is_on_floor():
		$texture.play("Jump")
	_get_input()
	

	
	
	if $pushRight.is_colliding():
		var object = $pushRight.get_collider()
		object.move_and_slide(Vector2(25,0) * move_speed * delta)
		is_pushing = true
		$texture.play("Push")
	elif $pushLeft.is_colliding():
		var object = $pushLeft.get_collider()
		object.move_and_slide(Vector2(-25,0) * move_speed * delta)
		is_pushing = true
		$texture.play("Push")
	else:
		is_pushing = false
	
			
		
	

		
	velocity = move_and_slide_with_snap(velocity, snap, UP)
	
	is_grounded = _check_is_ground()
			
func _get_input():

	if facing_right == true:
		$rui.scale.x = 1
	else:
		$rui.scale.x = -1
		
	if velocity.x > 1:
		$pushRight.set_enabled(true)
	else:
		$pushRight.set_enabled(false)

	if velocity.x < -1:
		$pushLeft.set_enabled(true)
	else:
		$pushLeft.set_enabled(false)
		
func _input(event: InputEvent) -> void: 
	snap = Vector2.UP
	if Input.is_action_just_pressed("jump") && JumpAvailability:
		$Jump.play()
#	if event.is_action_just_pressed("jump") && JumpAvailability || event.is_action_just_pressed("JumpW") && JumpAvailability || event.is_action_just_pressed("JumpUp") && JumpAvailability:  #Verificar se está no chão para poder pular apenas 1 vez
		velocity.y = jump_force / 2.1
		$texture.play("Jump")
		create_dust_particles()
	elif event.is_action_released("jump") && velocity.y < 0 && JumpAvailability:
		$texture.play("Jump")
		velocity.y = jump_force / 3
	

func _check_is_ground():
	for raycast in raycasts.get_children():
		if raycast.is_colliding(): 
			return true
	return false

func hurt():
		var pd = player_death.instance()
		pd.position = position
		get_tree().root.add_child(pd)
		set_physics_process(false)
		$rui.visible = false 
		var timer = get_tree().create_timer(2.0)
		yield(timer,"timeout")
#		pd.queue_free()
		get_tree().reload_current_scene()
		Global.batata = 0
		Global.cenoura = 0
		Global.cereja = 0
		
		


func completed():
	print(teste)
	set_physics_process(false)
	$rui.visible = false
	 

func cenouraPickUp():
	$Coin.play()
	Global.cenoura += 1
	print("Cenouras:",Global.cenoura)
	
func coinPickUp():
	$Coin.play()
	Global.batata += 1
	print("batata:",Global.batata)
	
func cerejaPickup():
	$Coin.play()
	Global.cereja += 1
	print("Cereja:",Global.cereja)
	


func _on_CoyoteTimer_timeout():
	print("timeout")
	JumpAvailability = false


func create_dust_particles():
	var dust_particles_instance = Global.instantiate(dust_particles_scene, self)
	dust_particles_instance.set_as_toplevel(true)
	if facing_right == true:
		dust_particles_instance.scale.x = 1
	else:
		dust_particles_instance.scale.x = -1
		dust_particles_place_holder.scale.x = -1
	dust_particles_instance.global_position = dust_particles_place_holder.global_position
	



	
