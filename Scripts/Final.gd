extends Node2D


func _process(delta):
	if Input.is_action_pressed("AllButtons"):
		$CanvasLayer/anime.play("dissolve")
		yield($CanvasLayer/anime, "animation_finished")
		get_tree().change_scene("res://Scenes/UI/Créditos.tscn")

func _ready():
	Music1.play_music3()
	Music1.stop_music()
