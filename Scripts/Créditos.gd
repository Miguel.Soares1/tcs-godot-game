extends Node2D

#func _process(delta):
#	if Input.is_action_pressed("AllButtons"):
#		get_tree().change_scene("res://Scenes/UI/TileScreen.tscn")

func _ready():
	$CanvasLayer/anime.play_backwards("dissolve")
	yield($CanvasLayer/anime, "animation_finished")


func _on_Timer_timeout():
	if Input.is_action_pressed("AllButtons"):
		$CanvasLayer/anime.play("dissolve")
		yield($CanvasLayer/anime, "animation_finished")
		get_tree().change_scene("res://Scenes/UI/TileScreen.tscn")
