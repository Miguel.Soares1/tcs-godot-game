extends Node2D

onready var historia = preload("res://Scenes/UI/História.tscn")

func _on_TextureButton_pressed():
		$CanvasLayer/anime.play("dissolve")
		yield($CanvasLayer/anime, "animation_finished")
		get_tree().change_scene_to(historia)


func _on_TextureButton3_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	get_tree().quit()


func _ready():
	$HBoxContainer/TextureButton.grab_focus()

func _on_TextureButton2_pressed():
	$CanvasLayer/anime.play("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	get_node("Keybinds/Panel").visible = true
