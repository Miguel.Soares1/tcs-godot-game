extends Node2D

onready var menu = preload("res://Scenes/UI/Menu level.tscn")

func _ready():
	$CanvasLayer/anime.play_backwards("dissolve")
	yield($CanvasLayer/anime, "animation_finished")
	pass



func _on_Timer_timeout():
	if Input.is_action_pressed("AllButtons"):
		$CanvasLayer/anime.play("dissolve")
		yield($CanvasLayer/anime, "animation_finished")
		get_tree().change_scene_to(menu)
		

