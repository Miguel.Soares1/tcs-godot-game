extends Control


var is_paused = false setget set_is_paused

func _ready():
	$VBoxContainer/TextureButton.grab_focus()	
func _unhandled_input(event):
	if event.is_action_pressed("Pause1"):
		self.is_paused = !is_paused
	if Input.get_action_strength("Restart"):
		get_tree().reload_current_scene()
		Global.batata = 0
		Global.cenoura = 0
		Global.cereja = 0
		

func set_is_paused(value):
	is_paused = value
	get_tree().paused = is_paused 
	visible = is_paused 


func _on_TextureButton_pressed():
	self.is_paused = false 
	



func _on_TextureButton2_pressed():
	self.is_paused = false
	Global.batata = 0
	Global.cenoura = 0
	Global.cereja = 0
	Music1.stop_music()
	get_tree().change_scene("res://Scenes/UI/Menu level.tscn")




func _on_TextureButton4_pressed():
	Global.batata = 0
	Global.cenoura = 0
	Global.cereja = 0
	get_tree().reload_current_scene()
	get_tree().paused = false

func _on_TextureButton3_pressed():
	get_tree().quit()


func _on_Menu_pressed():
	self.is_paused = !is_paused


func _on_TextureButton5_pressed():
	get_node("keybinds/Panel").visible = true 
	 

