extends Node


var back_music = load("res://Audio/pixel-perfect-112527.mp3") 
var music = load("res://Audio/music 8-bit.mp3")
var final =load("res://Audio/backfinal.mp3")

func _ready():
	pass
	
func play_music():
	$Music.stream = back_music 
	$Music.play()
	
func stop_music():
	$Music.stream = back_music 
	$Music.stop()
	
func lower_music():
	$Music.volume_db = -20
	
func reset():
	$Music.volume_db = -12
	
func play_music2():
	$Music2.stream = music
	$Music2.play()
	
func stop_music2():
	$Music2.stream = music
	$Music2.stop()

func lower_music2():
	$Music2.volume_db = -20
	
func reset2():
	$Music2.volume_db = -12
	
func play_music3():
	$Music3.stream = final
	$Music3.play()
	
func stop_music3():
	$Music3.stream = final
	$Music3.stop()
