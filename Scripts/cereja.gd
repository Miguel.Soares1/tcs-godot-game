extends Area2D


func _on_Cereja_body_entered(body):
	$AnimationPlayer.play("collected")
	if body.has_method("cerejaPickup"):
		body.cerejaPickup()
	

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "collected":
		queue_free()
