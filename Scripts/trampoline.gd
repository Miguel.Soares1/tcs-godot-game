extends Area2D

func _on_trampoline_body_entered(body):
	body.velocity.y = body.jump_force / 1.2
